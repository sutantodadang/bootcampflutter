import 'dart:io';

void main() {
  // soal 1
  stdout.writeln("Apakah anda mau menginstall dart? y/n");
  var jawaban = stdin.readLineSync();

  jawaban == 'y' ? print('Anda akan menginstall dart') : print("Aborted");

  // soal 2
  stdout.writeln("Masukkan Nama anda ");
  var nama = stdin.readLineSync();
  stdout.writeln("Masukkan Peran anda ");
  var peran = stdin.readLineSync();
  if (nama != '') {
    if (peran == '') {
      print("halo $nama isikan peran anda");
    } else if (nama == 'jane' && peran == 'penyihir') {
      print("""Selamat datang di Dunia Werewolf, Jane" 
    "Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!""");
    } else if (nama == 'jenita' && peran == 'guard') {
      print("""Selamat datang di Dunia Werewolf, Jenita" 
    "Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.""");
    } else if (nama == 'junaedi' && peran == 'werewolf') {
      print("""Selamat datang di Dunia Werewolf, Junaedi" 
    "Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!""");
    } else {
      print("maaf nama/peran tersebut tidak ditemukan");
    }
  } else if (nama == '') {
    print("Masukkan Nama Anda !!!");
  } else {
    print("niat main kagak!!!");
  }

  // soal 3
  stdout.writeln("Sekarang adalah hari ? ");
  var pilihanQuote = stdin.readLineSync();

  switch (pilihanQuote) {
    case 'senin':
      {
        print(
            "Segala sesuatu memiliki kesudahan, yang sudah berakhir biarlah berlalu dan yakinlah semua akan baik-baik saja.");
        break;
      }
    case 'selasa':
      {
        print(
            "Setiap detik sangatlah berharga karena waktu mengetahui banyak hal, termasuk rahasia hati.");
        break;
      }
    case 'rabu':
      {
        print(
            "Jika kamu tak menemukan buku yang kamu cari di rak, maka tulislah sendiri.");
        break;
      }
    case 'kamis':
      {
        print(
            "Jika hatimu banyak merasakan sakit, maka belajarlah dari rasa sakit itu untuk tidak memberikan rasa sakit pada orang lain.");
        break;
      }
    case 'jumat':
      {
        print("Hidup tak selamanya tentang pacar.");
        break;
      }
    case 'sabtu':
      {
        print("Rumah bukan hanya sebuah tempat, tetapi itu adalah perasaan.");
        break;
      }
    case 'minggu':
      {
        print(
            "Hanya seseorang yang takut yang bisa bertindak berani. Tanpa rasa takut itu tidak ada apapun yang bisa disebut berani.");
        break;
      }

      break;
    default:
      {
        print("tidak ada quote untuk hari tersebut");
      }
  }

  // soal 4

  const tanggal = 26;
  const bulan = 6;
  const tahun = 2021;

  switch (true) {
    case (tanggal < 1 || tanggal > 31):
      {
        print("tanggal di luar batas");
        break;
      }
    case (tahun < 1900 || tahun > 2200):
      {
        print("tahun di luar batas");
        break;
      }

    default:
      {
        switch (bulan) {
          case 1:
            {
              print("$tanggal Januari $tahun");
              break;
            }
          case 2:
            {
              print("$tanggal Februari $tahun");
              break;
            }
          case 3:
            {
              print("$tanggal Maret $tahun");
              break;
            }
          case 4:
            {
              print("$tanggal April $tahun");
              break;
            }
          case 5:
            {
              print("$tanggal Mei $tahun");
              break;
            }
          case 6:
            {
              print("$tanggal Juni $tahun");
              break;
            }
          case 7:
            {
              print("$tanggal Juli $tahun");
              break;
            }
          case 8:
            {
              print("$tanggal Agustus $tahun");
              break;
            }
          case 9:
            {
              print("$tanggal September $tahun");
              break;
            }
          case 10:
            {
              print("$tanggal Oktober $tahun");
              break;
            }
          case 11:
            {
              print("$tanggal November $tahun");
              break;
            }
          case 12:
            {
              print("$tanggal Desember $tahun");
              break;
            }
            break;
          default:
            {
              print("Diluar batas");
            }
        }
      }
  }
}
