import 'dart:core';

main(List<String> args) {
  Lingkaran lingkaran1;

  lingkaran1 = new Lingkaran();
  lingkaran1.alas = 20.0;
  lingkaran1.tinggi = 30.0;
  lingkaran1.setengah = 0.5;

  print(lingkaran1.hitungLuas());
}

class Lingkaran {
  double alas = 0;
  double tinggi = 0;
  double setengah = 0;

  double hitungLuas() {
    return this.alas * this.tinggi * this.setengah;
  }
}
