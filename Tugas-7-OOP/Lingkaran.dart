class Lingkaran {
  double _alas = 0;
  double _tinggi = 0;
  double _setengah = 0;

  void set alas(double value) {
    if (value < 0) {
      value *= -1;
    }

    _alas = value;
  }

  void set tinggi(double value) {
    if (value < 0) {
      value *= -1;
    }

    _tinggi = value;
  }

  void set setengah(double value) {
    if (value < 0) {
      value *= -1;
    }

    _setengah = value;
  }

  double get alas {
    return _alas;
  }

  double get tinggi {
    return _tinggi;
  }

  double get setengah {
    return _setengah;
  }

  double get luas => _alas * _setengah * _tinggi;
}
