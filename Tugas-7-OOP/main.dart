import 'dart:core';
import 'Lingkaran.dart';

void main(List<String> args) {
  Lingkaran lingkaran;

  double luasLingkaran;

  lingkaran = new Lingkaran();
  lingkaran.alas = 30.0;
  lingkaran.tinggi = 50.0;
  lingkaran.setengah = 0.5;

  luasLingkaran = lingkaran.luas;
  print(luasLingkaran);
}
