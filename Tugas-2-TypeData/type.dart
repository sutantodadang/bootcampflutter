import 'dart:io';
import 'dart:core';

void main() {
  // soal 1
  var word = 'dart';
  var second = 'is';
  var third = 'awesome';
  var fourth = 'and';
  var fifth = 'I';
  var sixth = 'love';
  var seventh = 'it!';

  var kalimatGabungan = '$word $second $third $fourth $fifth $sixth $seventh';

  print(kalimatGabungan);

  // soal 2
  var sentence = "I am going to be Flutter Developer";

  var exampleFirstWord = sentence[0];
  var exampleSecondWord = sentence[2] + sentence[3];
  var thirdWord = sentence[5] +
      sentence[6] +
      sentence[7] +
      sentence[8] +
      sentence[9]; // lakukan sendiri
  var fourthWord = sentence[11] + sentence[12]; // lakukan sendiri
  var fifthWord = sentence[14] + sentence[15]; // lakukan sendiri
  var sixthWord = sentence[17] +
      sentence[18] +
      sentence[19] +
      sentence[20] +
      sentence[21] +
      sentence[22] +
      sentence[23]; // lakukan sendiri
  var seventhWord = sentence[25] +
      sentence[26] +
      sentence[27] +
      sentence[28] +
      sentence[29] +
      sentence[30] +
      sentence[31] +
      sentence[32] +
      sentence[33]; // lakukan sendiri

  print('First Word: ' + exampleFirstWord);
  print('Second Word: ' + exampleSecondWord);
  print('Third Word: ' + thirdWord);
  print('Fourth Word: ' + fourthWord);
  print('Fifth Word: ' + fifthWord);
  print('Sixth Word: ' + sixthWord);
  print('Seventh Word: ' + seventhWord);

// soal 3
  stdout.writeln('masukan nama depan : ');
  var namaDepan = stdin.readLineSync();
  stdout.writeln('masukan nama belakang : ');
  var namaBelakang = stdin.readLineSync();
  print('nama lengkap anda adalah: $namaDepan $namaBelakang');

// soal 4

  var a = 5;
  var b = 10;

  var perkalian = a * b;
  var pembagian = a / b;
  var pertambahan = a + b;
  var pengurangan = a - b;

  print('perkalian : $perkalian');
  print('pembagian : $pembagian');
  print('pertambahan : $pertambahan');
  print('pengurangan : $pengurangan');
}
