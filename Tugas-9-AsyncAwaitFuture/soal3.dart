import 'dart:async';
import 'dart:core';

void main(List<String> args) async {
  print("Ready... Sing");
  await line();
  await line2();
  await line3();
  await line4();
}

Future<void> line() async {
  String line = "pernahkan kau merasa, ";
  return await Future.delayed(Duration(seconds: 5), () => print(line));
}

Future<void> line2() async {
  String line = "pernahkah kau merasa ........... ";
  return await Future.delayed(Duration(seconds: 3), () => print(line));
}

Future<void> line3() async {
  String line = "pernahkah kau merasa,";
  return await Future.delayed(Duration(seconds: 2), () => print(line));
}

Future<void> line4() async {
  String line =
      "hatimu hampa pernahkah kau merasa hati mu kosong ............,";
  return await Future.delayed(Duration(seconds: 1), () => print(line));
}
