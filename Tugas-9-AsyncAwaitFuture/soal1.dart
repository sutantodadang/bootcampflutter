import 'dart:core';
import 'dart:async';

void main(List<String> args) async {
  var h = Human();

  print("name 1 : luffy");
  print("name 2 : zoro");
  print("name 3 : killer");
  print(await h.getData());
  // await h.getData();
  print(h.name);
}

class Human {
  String name = "nama character onepiece";

  Future<String> getData() async {
    await Future.delayed(Duration(seconds: 3));
    name = "name 4 : dadang";

    return "get data [done]";
  }
}
