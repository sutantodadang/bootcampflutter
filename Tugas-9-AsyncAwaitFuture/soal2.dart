import 'dart:core';

main(List<String> args) {
  print("life");
  delayedPrint(2, "never flat").then((value) => print(value));
  print("is");
}

Future delayedPrint(int detik, String pesan) {
  final durasi = Duration(seconds: detik);
  return Future.delayed(durasi).then((value) => pesan);
}
