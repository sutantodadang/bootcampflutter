import 'dart:core';

void main() {
  teriak();

  var num1 = 12;
  var num2 = 4;

  var hasilkali = kalikan(num1, num2);
  print(hasilkali);

  var name = "Agus";
  var age = 30;
  var address = "Jln. Malioboro, Yogyakarta";
  var hobby = "Gaming";

  var perkenalan = introduce(name, age, address, hobby);
  print(perkenalan);

  var factor = factorial(6);
  print(factor);
}

// soal 1
teriak() {
  print("Haloo Sanbers!!");
}

// soal 2
kalikan(num1, num2) {
  return num1 * num2;
}

// soal 3
introduce(name, age, address, hobby) {
  return "Nama Saya $name, Umur Saya $age Tahun, Alamat Saya di $address, dan Saya Punya Hobi $hobby";
}

// soal 4
factorial(n) {
  if (n == 1) {
    return n;
  } else {
    return n * factorial(n - 1);
  }
}
