import 'dart:core';
import 'dart:io';


void main(){
  // soal 1 
  var i = 2;
  var loop1 = "";
  
  while (i < 20) {
    loop1 += "$i - I Love Coding \n";
    i += 2;
  }


  var j = 20;
  var loop2 = "";
  
  while (j>0) {
    loop2 += "$j - I Will Become Mobile Developer \n";
    j -= 2;
  }
  print("Looping Pertama");
  print(loop1);
  print("Looping Kedua");
  print(loop2);

  // soal 2 

  var forloop = "";
  for (var i = 1; i < 20; i++) {
    if (i % 2 != 0 && i % 3 == 0) {
      forloop += "$i - I Love Coding \n";
    } else if (i % 2 ==0){
      forloop += "$i - Berkualitas \n";
    } else {
      forloop += "$i - Santai \n";
    }
  }
  print(forloop);

  // soal 3 
  print("soal 3 \n");
  for (var i = 1; i <= 4; i++) {
   
    for (var j = 1; j < 8; j++) {
     
      stdout.write("#");
    }
    stdout.write("#\n");
  }

  // soal 4 
  print("\n");
  print("Soal 4 \n");
  for (var x = 1; x <= 7; x++) {
    for (var y = 1; y <= x; y++) {
      stdout.write("#");
    }
    stdout.write(" \n");
  }


}