class Titan {
  double _powerpoint = 0;

  double get powerpoint => _powerpoint;
  set powerpoint(value) {
    if (value < 5) {
      value = 10;
    }
    _powerpoint = value;
  }

  String tampilkanPower() => _powerpoint.toString();
}
