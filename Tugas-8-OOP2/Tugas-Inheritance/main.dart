import 'dart:core';
import 'armor_titan.dart';
import 'attack_titan.dart';
import 'beast_titan.dart';
import 'human.dart';

void main(List<String> args) {
  ArmorTitan armorTitan = new ArmorTitan();
  AttackTitan attackTitan = new AttackTitan();
  BeastTitan beastTitan = new BeastTitan();
  Human human = new Human();

  armorTitan.powerpoint = 10.0;
  attackTitan.powerpoint = 10.0;
  beastTitan.powerpoint = 10.0;
  human.powerpoint = 10.0;

  print(armorTitan.terjang());
  print(armorTitan.tampilkanPower());
}
