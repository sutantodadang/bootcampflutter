import 'dart:core';
import 'employee.dart';

void main(List<String> args) {
  var id = new Employee.idPegawai(0912301823);
  var nama = new Employee.nama("Dadang");
  var posisi = new Employee.posisi("programmer");
  var gaji = new Employee.gaji(123000000.100);
  var alamat = new Employee.alamat("Kota Surabaya");

  print(id.idPegawai);
  print(nama.nama);
  print(posisi.posisi);
  print(gaji.gaji);
  print(alamat.alamat);
}
