import 'dart:core';
import 'bangun-datar.dart';

class Segitiga extends BangunDatar {
  double alas = 0;
  double tinggi = 0;
  double setengah = 0;

  Segitiga(double alas, double tinggi, double setengah) {
    this.alas = alas;
    this.tinggi = tinggi;
    this.setengah = setengah;
  }

  @override
  double luas() {
    return setengah * alas * tinggi;
  }

  double keliling() {
    return setengah + alas + tinggi;
  }
}
