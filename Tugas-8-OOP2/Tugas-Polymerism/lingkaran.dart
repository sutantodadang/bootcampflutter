import 'dart:core';
import 'bangun-datar.dart';

class Lingkaran extends BangunDatar {
  double jari = 0;
  double pi = 0;
  double dua = 0;

  Lingkaran(double jari, double pi, double dua) {
    this.jari = jari;
    this.pi = pi;
    this.dua = dua;
  }

  @override
  double luas() {
    return pi * jari * jari;
  }

  double keliling() {
    return dua * pi + jari;
  }
}
