import 'dart:core';
import 'bangun-datar.dart';
import 'segitiga.dart';
import 'lingkaran.dart';
import 'persegi.dart';

void main(List<String> args) {
  BangunDatar bangunDatar = new BangunDatar();
  Segitiga segitiga = new Segitiga(20.0, 40.0, 0.5);
  Lingkaran lingkaran = new Lingkaran(35, 3.14, 2.0);
  Persegi persegi = new Persegi(20.0, 4);

  bangunDatar.keliling();
  bangunDatar.luas();

  print("luas segitiga ialah : ${segitiga.luas()}");
  print("keliling segitiga ialah : ${segitiga.keliling()}");

  print("luas lingkaran ialah : ${lingkaran.luas()}");
  print("keliling lingkaran ialah : ${lingkaran.keliling()}");

  print("luas persegi ialah : ${persegi.luas()}");
  print("keliling persegi ialah : ${persegi.keliling()}");
}
