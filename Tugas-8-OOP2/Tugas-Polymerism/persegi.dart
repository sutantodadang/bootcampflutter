import 'dart:core';
import 'bangun-datar.dart';

class Persegi extends BangunDatar {
  double sisi = 0;
  double empat = 0;

  Persegi(double sisi, double empat) {
    this.sisi = sisi;
    this.empat = empat;
  }

  @override
  double luas() {
    return sisi * sisi;
  }

  double keliling() {
    return empat * sisi;
  }
}
