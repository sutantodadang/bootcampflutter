import 'dart:convert';

class Chats {
  final String name, message, time, image;
  Chats(this.name, this.message, this.time, this.image);
}

class ChatModel {
  final name, message, time, image, id, createdAt;
  ChatModel(
      {this.name,
      this.message,
      this.time,
      this.image,
      this.id,
      this.createdAt});

  factory ChatModel.fromJson(Map<String, dynamic> json) => ChatModel(
        name: json["name"],
        message: json["message"],
        time: json["time"],
        image: json["image"],
        id: json["id"],
        createdAt: DateTime.parse(json["createdAt"]),
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "message": message,
        "time": time,
        "image": image,
        "id": id,
        "createdAt": createdAt,
      };
}

ChatModel chatModelFromJson(String str) => ChatModel.fromJson(json.decode(str));
String chatModelToJson(ChatModel data) => json.encode(data.toJson());
