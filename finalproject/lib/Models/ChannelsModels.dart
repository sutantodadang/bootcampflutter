import 'dart:convert';

class Channels {
  final String name, image, desc;
  Channels(this.name, this.desc, this.image);
}

class ChannelsModels {
  final name, desc, image, id, createdAt;
  ChannelsModels({this.name, this.desc, this.image, this.id, this.createdAt});

  factory ChannelsModels.fromJson(Map<String, dynamic> json) => ChannelsModels(
        name: json["name"],
        desc: json["desc"],
        image: json["image"],
        id: json["id"],
        createdAt: DateTime.parse(json["createdAt"]),
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "desc": desc,
        "image": image,
        "id": id,
        "createdAt": createdAt,
      };
}

ChannelsModels channelsModelFromJson(String str) =>
    ChannelsModels.fromJson(json.decode(str));
String chatModelToJson(ChannelsModels data) => json.encode(data.toJson());
