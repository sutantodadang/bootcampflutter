import 'dart:convert';
import 'package:finalproject/Models/ChannelsModels.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class ChannelsPage extends StatefulWidget {
  @override
  _ChannelsPageState createState() => _ChannelsPageState();
}

class _ChannelsPageState extends State<ChannelsPage> {
  get id => null;

  getChannelData() async {
    var response = await http.get(
        Uri.parse("https://mocki.io/v1/b845ffe9-dab2-44c6-b12a-b81c68121a79"));

    var jsonData = jsonDecode(response.body);

    List<Channels> channels = [];

    for (var u in jsonData) {
      Channels channel = Channels(u["name"], u["desc"], u["image"]);
      channels.add(channel);
    }

    print(channels.length);
    return channels;
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    setState(() {
      getChannelData();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(children: <Widget>[
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SafeArea(
              child: Padding(
                padding: EdgeInsets.only(left: 16, right: 16, top: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "Channels",
                      style:
                          TextStyle(fontSize: 38, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              child: Padding(
                padding: EdgeInsets.only(top: 16, left: 16, right: 16),
                child: TextField(
                  decoration: InputDecoration(
                    hintText: "Search...",
                    hintStyle: TextStyle(color: Colors.grey.shade600),
                    prefixIcon: Icon(
                      Icons.search,
                      color: Colors.grey.shade600,
                      size: 20,
                    ),
                    filled: true,
                    fillColor: Colors.grey.shade100,
                    contentPadding: EdgeInsets.all(8),
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20),
                        borderSide: BorderSide(color: Colors.grey.shade100)),
                  ),
                ),
              ),
            ),
            Card(
              child: FutureBuilder(
                future: getChannelData(),
                builder: (context, snapshot) {
                  if (snapshot.data == null) {
                    return Container(
                      child: Center(
                        child: Text("Loading...."),
                      ),
                    );
                  } else {
                    return Expanded(
                      child: ListView.builder(
                          shrinkWrap: true,
                          itemCount: snapshot.data.length,
                          itemBuilder: (context, i) {
                            return ListTile(
                              leading: CircleAvatar(
                                radius: 48,
                                backgroundImage:
                                    NetworkImage(snapshot.data[i].image),
                              ),
                              title: Text(snapshot.data[i].name),
                              subtitle: Text(snapshot.data[i].desc),
                            );
                          }),
                    );
                  }
                },
              ),
            ),
          ],
        ),
      ]),
    );
  }
}
