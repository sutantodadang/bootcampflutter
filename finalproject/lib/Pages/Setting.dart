import 'package:flutter/material.dart';

class Setting extends StatefulWidget {
  @override
  _SettingState createState() => _SettingState();
}

class _SettingState extends State<Setting> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Settings"),
        ),
        body: SafeArea(
            child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: 50),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                      width: 100,
                      child: Image.network("https://i.pravatar.cc/300")),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "Dadang Sutanto",
                      style: TextStyle(color: Colors.blue.shade400),
                    ),
                  )
                ],
              ),
            )
          ],
        )));
  }
}
