import 'package:finalproject/DrawerPage.dart';
import 'package:finalproject/Models/ChatModels.dart';
import 'package:finalproject/Pages/ChatDetailPage.dart';
import 'package:finalproject/Pages/LoginPage.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

class Chat extends StatefulWidget {
  @override
  _ChatState createState() => _ChatState();
}

class _ChatState extends State<Chat> {
  Future<void> _signOut() async {
    await FirebaseAuth.instance.signOut();
  }

  get id => null;

  getUserData() async {
    var response = await http.get(
        Uri.parse("https://mocki.io/v1/33fc387f-6c2d-4f6e-82fd-e450eba61dee"));

    var jsonData = jsonDecode(response.body);

    List<Chats> chats = [];

    for (var u in jsonData) {
      Chats chat = Chats(u["name"], u["message"], u["time"], u["image"]);
      chats.add(chat);
    }

    print(chats.length);
    return chats;
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    setState(() {
      getUserData();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Telegrim"),
        actions: <Widget>[
          ElevatedButton.icon(
            onPressed: () {
              _signOut().then(
                (value) => Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (context) => LoginPage(),
                  ),
                ),
              );
            },
            icon: Icon(Icons.exit_to_app),
            label: Text("Sign Out"),
            style: ElevatedButton.styleFrom(elevation: 0),
          )
        ],
      ),
      drawer: DrawerPage(),
      body: Card(
        child: FutureBuilder(
          future: getUserData(),
          builder: (context, snapshot) {
            if (snapshot.data == null) {
              return Container(
                child: Center(
                  child: Text("Loading...."),
                ),
              );
            } else {
              return ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (context, i) {
                    return ListTile(
                      leading: CircleAvatar(
                        radius: 24,
                        backgroundImage: NetworkImage(snapshot.data[i].image),
                      ),
                      title: Text(snapshot.data[i].name),
                      subtitle: Text(snapshot.data[i].message),
                      trailing: Text(snapshot.data[i].time),
                    );
                  });
            }
          },
        ),
      ),
    );
  }
}
