import 'dart:core';

void main() {
  print(range(1, 10));
  print(rangeWithStep(10));

  //contoh input
  var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
  ];

  print(dataHandling(input));

  print(balikKata("Kasur")); // rusaK
  print(balikKata("SanberCode")); // edoCrebnaS
  print(balikKata("Haji")); // hajI
  print(balikKata("racecar")); // racecar
  print(balikKata("Sanbers")); // srebnaS
}

range(start, finish) {
  if (start > finish) {
    var list = [for (var i = start; i <= finish; i++) i];
    return list.reversed;
  } else {
    var list = [for (var i = start; i <= finish; i++) i];
    return list;
  }
}

rangeWithStep(stop, {start: 1, step: 1}) {
  if (step == 0) throw Exception("Step cannot be 0");

  if (start > stop) {
    return start < stop == step > 0
        ? List<int>.generate(((start - stop) / 3).abs().ceil().reversed,
            (int i) => start + (i * 3))
        : [];
  }

  return start < stop == step > 0
      ? List<int>.generate(
          ((start - stop) / step).abs().ceil(), (int i) => start + (i * step))
      : [];
}

dataHandling(array) {
  var data = "";
  for (var i = 0; i < array.length; i++) {
    data += "Nomor ID : " +
        array[i][0] +
        "\n" +
        "Nama Lengkap : " +
        array[i][1] +
        "\n" +
        "TTL : " +
        array[i][2] +
        ", " +
        array[i][3] +
        "\n" +
        "Hobi : " +
        array[i][4] +
        "\n\n";
  }
  return data;
}

balikKata(input) {
  var balik = "";
  for (var i = input.length - 1; i >= 0; i--) {
    balik += input[i];
  }

  return balik;
}
