import 'dart:core';
import 'dart:math';
import 'dart:async';

class Point {
  double x = 0, y = 0;
  Point(this.x, this.y);

  static double distanceBetween(Point a, Point b) {
    var dx = a.x - b.x;
    var dy = a.y - b.y;
    return sqrt(dx * dx + dy * dy);
  }
}

void main() async {
  var a = Point(2, 2);
  var b = Point(4, 4);

  var distance = Point.distanceBetween(a, b);

  assert(2.8 < distance && distance < 2.9);
  print(distance);
  print("starting");
  var timer = Timer(Duration(seconds: -3), () => print("timer completed"));
  print("finished");
}
