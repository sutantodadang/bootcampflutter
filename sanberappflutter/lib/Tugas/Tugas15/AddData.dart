import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sanberappflutter/Tugas/Tugas15/Home.dart';
import 'package:sanberappflutter/Tugas/Tugas15/Models/user_modal.dart';

class AddData extends StatefulWidget {
  final String title;
  AddData({Key key, this.title}) : super(key: key);

  @override
  _AddData createState() => _AddData();
}

Future<UserModel> createUser(String name, String email, String address) async {
  var apiUrl = Uri.parse("https://achmadhilmy-sanbercode.my.id/api/v1/profile");
  final response = await http
      .post(apiUrl, body: {"name": name, "email": email, "address": address});

  if (response.statusCode == 201) {
    final String responseString = response.body;

    return userModelFromJson(responseString);
  } else {
    return null;
  }
}

class _AddData extends State<AddData> {
  UserModel _user;
  final TextEditingController nameController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController addressController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Tambah Data"),
      ),
      body: Container(
        child: Padding(
          padding: EdgeInsets.all(32),
          child: Column(
            children: <Widget>[
              TextField(
                controller: nameController,
                decoration: InputDecoration(
                  labelText: "Nama User",
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              TextField(
                controller: emailController,
                decoration: InputDecoration(
                  labelText: "Email User",
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              TextField(
                controller: addressController,
                decoration: InputDecoration(
                  labelText: "Alamat User",
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
              ),
              SizedBox(
                height: 36,
              ),
              _user == null
                  ? Container()
                  : Text(
                      "The user ${_user.name} is created, and id ${_user.id}"),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          final String name = nameController.text;
          final String email = emailController.text;
          final String address = addressController.text;
          final UserModel user = await createUser(name, email, address);
          setState(() {
            _user = user;
          });
          Navigator.of(context).pop(Home());
        },
        tooltip: "Increment",
        child: Icon(Icons.add),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}
