import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:sanberappflutter/Latihan/Latihan16/Auth/LoginPage.dart';
import 'package:sanberappflutter/Tugas/Tugas15/AddData.dart';
import 'package:sanberappflutter/Tugas/Tugas15/DrawerPage.dart';
import 'package:sanberappflutter/Tugas/Tugas15/Models/user_modal.dart';

class Chat extends StatefulWidget {
  @override
  _ChatState createState() => _ChatState();
}

class _ChatState extends State<Chat> {
  Future<void> _signOut() async {
    await FirebaseAuth.instance.signOut();
  }

  get id => null;

  getUserData() async {
    var response = await http
        .get(Uri.parse("https://achmadhilmy-sanbercode.my.id/api/v1/profile"));

    var jsonData = jsonDecode(response.body);

    List<Users> users = [];

    for (var u in jsonData) {
      Users user = Users(u["name"], u["email"], u["address"]);
      users.add(user);
    }

    print(users.length);
    return users;
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    setState(() {
      getUserData();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Telegrim"),
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 20.0),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(elevation: 0),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => AddData(),
                  ),
                );
              },
              child: Icon(
                Icons.add,
                size: 26,
              ),
            ),
          ),
          ElevatedButton.icon(
            onPressed: () {
              _signOut().then(
                (value) => Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (context) => LoginPage(),
                  ),
                ),
              );
            },
            icon: Icon(Icons.power_off),
            label: Text("Sign Out"),
            style: ElevatedButton.styleFrom(elevation: 0),
          )
        ],
      ),
      drawer: DrawerPage(),
      body: Card(
        child: FutureBuilder(
          future: getUserData(),
          builder: (context, snapshot) {
            if (snapshot.data == null) {
              return Container(
                child: Center(
                  child: Text("Loading...."),
                ),
              );
            } else {
              return ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (context, i) {
                    return ListTile(
                      title: Text(snapshot.data[i].name),
                      subtitle: Text(snapshot.data[i].address),
                      trailing: Text(snapshot.data[i].email),
                    );
                  });
            }
          },
        ),
      ),
    );
  }
}
