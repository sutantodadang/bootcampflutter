import 'package:flutter/material.dart';
import 'DrawerScreen.dart';
import 'Models/Chart_model.dart';

class ChatScreen extends StatefulWidget {
  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Chat Screen"),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Icon(Icons.search),
          )
        ],
      ),
      drawer: DrawerScreen(),
      // <!--  //? #Soal No 3 (15 poin) -- ChatScreen.dart --
      //? Buatlah ListView widget agar dapat menampilkan list title, subtitle dan trailling, dan styling agar dapat
      //tampil baik di device -->
      //Tuliskan coding disini//
      body: ListView.separated(
          itemBuilder: (ctx, i) {
            return ListTile(
              leading: CircleAvatar(
                radius: 24,
                backgroundImage: NetworkImage(data[i].profileUrl),
              ),
              title: Text(data[i].name),
              subtitle: Text(data[i].message),
              trailing: Text(data[i].time),
            );
          },
          separatorBuilder: (ctx, i) => Divider(),
          itemCount: data.length),
      //end coding
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.create, color: Colors.white),
        backgroundColor: Color(0xFF65a9e0),
        onPressed: () {},
      ),
    );
  }
}
