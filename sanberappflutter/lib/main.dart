import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:sanberappflutter/Latihan/Latihan16/Auth/LoginPage.dart';
import 'package:sanberappflutter/Latihan/Latihan17/Bloc/AppMain.dart';
import 'package:sanberappflutter/Quiz3/MainApp.dart';
import 'package:sanberappflutter/Tugas/Tugas12/Telegram.dart';
import 'package:sanberappflutter/Tugas/Tugas13/HomeScreen.dart';
import 'package:sanberappflutter/Tugas/Tugas14/Get_Data.dart';
import 'package:sanberappflutter/Tugas/Tugas15/LoginScreen.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Telegram Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: AppMain(),
    );
  }
}
