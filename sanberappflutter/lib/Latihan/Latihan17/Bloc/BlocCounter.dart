import 'dart:async';
import 'dart:core';

import 'package:sanberappflutter/Latihan/Latihan17/Bloc/EventManager.dart';

class BlocCounter {
  int _counter = 0;
  final _counterStateController = StreamController<int>();

  StreamSink<int> get _inCounter => _counterStateController.sink;

  Stream<int> get counter => _counterStateController.stream;

  final _counterEventController = StreamController<EventManager>();

  Sink<EventManager> get counterEventSink => _counterEventController.sink;

  BlocCounter() {
    _counterEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(EventManager eventManager) {
    if (eventManager is IncrementEvent) {
      _counter++;
    } else {
      _counter--;
    }
    _inCounter.add(_counter);
  }

  void dispose() {
    _counterEventController.close();
    _counterStateController.close();
  }
}
