import 'package:flutter/material.dart';
import 'package:sanberappflutter/Latihan/Latihan17/Bloc/BlocCounter.dart';
import 'package:sanberappflutter/Latihan/Latihan17/Bloc/EventManager.dart';

class AppMain extends StatefulWidget {
  @override
  _AppMainState createState() => _AppMainState();
}

class _AppMainState extends State<AppMain> {
  final _bloc = BlocCounter();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("BLOC State"),
      ),
      body: Center(
        child: StreamBuilder(
          stream: _bloc.counter,
          initialData: 0,
          builder: (BuildContext context, AsyncSnapshot<int> snip) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Image.network(
                  "https://raw.githubusercontent.com/felangel/bloc/master/docs/assets/bloc_logo_full.png",
                  width: 250,
                  height: 250,
                ),
                Text("Press Buttton Bellow"),
                Text("${snip.data}"),
              ],
            );
          },
        ),
      ),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          FloatingActionButton(
            onPressed: () {
              _bloc.counterEventSink.add(IncrementEvent());
            },
            child: Icon(Icons.add),
            backgroundColor: Colors.green,
          ),
          SizedBox(
            width: 10,
          ),
          FloatingActionButton(
            onPressed: () {
              _bloc.counterEventSink.add(DecrementEvent());
            },
            child: Icon(Icons.remove),
            backgroundColor: Colors.red,
          ),
        ],
      ),
    );
  }
}
